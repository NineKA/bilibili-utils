#!/usr/bin/env python3

import re
import os
from sys import argv


class Task:
    def __init__(self, task_str) -> None:
        task_regex = r'^(BV[0-9a-zA-Z]+)\s+([0-9]+)\s+([^\n]+)$'
        result = re.match(task_regex, task_str)
        self.bvid = result.group(1)
        self.part = result.group(2)
        self.name = result.group(3).strip()


def load_tasks(path):
    file = open(path, 'r')
    tasks = []
    for line in file:
        tasks.append(Task(line))
    file.close()
    return tasks


def download_video(task):
    command = 'bilibili-dl'
    command += ' --bvid {}'.format(task.bvid)
    command += ' --part {}'.format(task.part)
    command += ' --h265'
    command += ' --ao "{}.m4a"'.format(task.name)
    command += ' --vo "{}.m4v"'.format(task.name)
    print(command)
    return os.system(command) == 0


def cleanup_video(task):
    if os.path.exists('{}.m4a'.format(task.name)):
        os.remove('{}.m4a'.format(task.name))
    if os.path.exists('{}.m4v'.format(task.name)):
        os.remove('{}.m4v'.format(task.name))


def download_danmaku(task):
    command = 'danmakuconv'
    command += ' --bvid {}'.format(task.bvid)
    command += ' --part {}'.format(task.part)
    command += ' -o "subtitles/{}.ass"'.format(task.name)
    print(command)
    return os.system(command) == 0


def cleanup_danmaku(task):
    if os.path.exists('subtitles/{}.ass'.format(task.name)):
        os.remove('subtitles/{}.ass'.format(task.name))


if __name__ == '__main__':
    tasks = load_tasks(argv[1])
    if not os.path.isdir('subtitles'):
        os.mkdir('subtitles')

    failed_video_tasks = []
    failed_danmaku_tasks = []

    for task in tasks:
        if not os.path.exists('{}.mkv'.format(task.name)):
            if not download_video(task):
                cleanup_video(task)
                failed_video_tasks.append(task)

    for task in tasks:
        if not os.path.exists('subtitles/{}.ass'.format(task.name)):
            if not download_danmaku(task):
                cleanup_danmaku(task)
                failed_danmaku_tasks.append(task)

    if len(failed_video_tasks) != 0:
        print("Failed Video Downloads:")
        for task in failed_video_tasks:
            print('{} P{} {}'.format(task.bvid, task.part, task.name))

    if len(failed_danmaku_tasks) != 0:
        print("Failed Danmaku Downloads:")
        for task in failed_danmaku_tasks:
            print('{} P{} {}'.format(task.bvid, task.part, task.name))
