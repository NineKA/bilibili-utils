#!/usr/bin/env python3

import os
import subprocess


def get_candidate_video_list():
    current_dir = os.getcwd()
    found_files = {}
    for filename in os.listdir(current_dir):
        filename = os.path.join(current_dir, filename)
        if not os.path.isfile(filename):
            continue
        [filename, extension] = os.path.splitext(filename)
        if not ((extension == '.m4v') or (extension == '.m4a')):
            continue
        if filename in found_files.keys():
            found_files[filename] = found_files[filename] + 1
        else:
            found_files[filename] = 1
    found_files = [k for (k, v) in found_files.items() if v == 2]
    return found_files


if __name__ == '__main__':
    candidate_videos = get_candidate_video_list()
    candidate_videos.sort()
    for video in candidate_videos:
        print(video + ".mkv")

        video_file = video + '.m4v'
        audio_file = video + '.m4a'
        aria2_file = video + '.txt'

        output_file = video + '.mkv'

        pv_command = ['pv', video_file]
        ffmpeg_command = [
            'ffmpeg',
            '-i', video_file, '-i', audio_file,
            '-c', 'copy',
            '-v', 'warning',
            output_file]
        ffmpeg_proc = subprocess.run(ffmpeg_command)
        assert(ffmpeg_proc.returncode == 0)
        os.remove(video_file)
        os.remove(audio_file)

        if os.path.isfile(aria2_file):
            os.remove(aria2_file)
