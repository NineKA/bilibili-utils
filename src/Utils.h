#ifndef BILIBILIUTILS_UTILS_H
#define BILIBILIUTILS_UTILS_H

#include <string>

namespace bilibiliutils::utils {
std::string formatHumanReadableSpeed(std::size_t BytesPerSecond);
unsigned checkAvID(std::string_view AvIDString);
std::string checkBvID(std::string_view BvIDString);

std::string formatIndex(unsigned TotalCount, unsigned Index);
} // namespace bilibiliutils::utils

#endif