#include "HTTPClient.h"
#include "Exceptions.h"

#include <curlpp/Easy.hpp>
#include <curlpp/Infos.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/cURLpp.hpp>
#include <fmt/format.h>
#include <toml++/toml.h>

#include <unordered_map>

__attribute__((constructor)) void initialize() { aria2::libraryInit(); }
__attribute__((destructor)) void destructor() { aria2::libraryDeinit(); }

namespace bilibiliutils {
HTTPClient::Aria2DownloadTask::Aria2DownloadTask(
    std::string_view URL, std::string_view Path)
    : mURL(URL), mPath(Path) {}

std::string HTTPClient::get(std::string_view URL) const {
  std::string OwnedURL(URL.cbegin(), URL.cend());
  std::ostringstream ResponseStream;
  curlpp::Easy Request;

  Request.setOpt<curlpp::Options::Url>(OwnedURL);
  Request.setOpt<curlpp::Options::HttpGet>(true);
  Request.setOpt<curlpp::Options::WriteStream>(&ResponseStream);

  if (!mUserAgent.empty())
    Request.setOpt<curlpp::Options::UserAgent>(mUserAgent);

  if (!mUserAgent.empty() && !mSESSDATA.empty()) {
    auto CookieString =
        fmt::format("DedeUserID={}; SESSDATA={};", mDedeUserID, mSESSDATA);
    Request.setOpt<curlpp::Options::Cookie>(CookieString);
  }

  Request.perform();

  auto ResponseCode = curlpp::infos::ResponseCode::get(Request);
  if (ResponseCode != 200) throw HTTPError(OwnedURL, ResponseCode);

  return ResponseStream.str();
}

HTTPClient::Aria2DownloadTask *
HTTPClient::addAria2Task(std::string_view URL, std::string_view Path) {
  auto &&TaskOwnedPtr =
      mTasks.emplace_back(std::make_unique<Aria2DownloadTask>(URL, Path));
  return TaskOwnedPtr.get();
}

void HTTPClient::Ignore(
    Aria2DownloadTask *DownloadTask, aria2::DownloadHandle *Handle) {
  (void)DownloadTask;
  (void)Handle;
}

void HTTPClient::runAria2Tasks(CallbackFn const &Callback) {
  std::unordered_map<Aria2DownloadTask *, aria2::A2Gid> GIDMap;

  aria2::SessionConfig Config;
  aria2::KeyVals SessionOptions;
  SessionOptions.push_back(
      {"max-connection-per-server",
       fmt::format("{}", Aria2MaxConnectionPerServer)});
  SessionOptions.push_back(
      {"max-concurrent-downloads",
       fmt::format("{}", Aria2MaxConcurrentDownloads)});
  SessionOptions.push_back({"split", fmt::format("{}", AriaSplit)});

  aria2::Session *Aria2Session = aria2::sessionNew(SessionOptions, Config);
  if (Aria2Session == nullptr) {
    throw std::runtime_error("cannot create aria2 session");
  }

  for (auto const &Task : mTasks) {
    GIDMap.insert({Task.get(), {}});
    aria2::KeyVals TaskOptions;

    if (!mUserAgent.empty()) {
      TaskOptions.push_back({"user-agent", mUserAgent});
    }

    TaskOptions.push_back({"referer", "https://www.bilibili.com"});
    TaskOptions.push_back({"out", std::string(Task->getPath())});

    auto *GID    = &GIDMap[Task.get()];
    auto TaskURL = std::string(Task->getURL());
    if (aria2::addUri(Aria2Session, GID, {TaskURL}, TaskOptions) < 0)
      throw std::runtime_error("cannot add aria2 download task");
  }

  for (;;) {
    auto ReturnCode = aria2::run(Aria2Session, aria2::RUN_ONCE);
    for (auto &&[TaskPtr, GID] : GIDMap) {
      auto *Aria2Handle = aria2::getDownloadHandle(Aria2Session, GID);
      Callback(TaskPtr, Aria2Handle);
    }
    if (ReturnCode != 1) break;
  }

  aria2::sessionFinal(Aria2Session);
}

void HTTPClient::setLogin(
    std::string_view DedeUserID, std::string_view SESSDATA) {
  mDedeUserID = std::string(DedeUserID.cbegin(), DedeUserID.cend());
  mSESSDATA   = std::string(SESSDATA.cbegin(), SESSDATA.cend());
}

void HTTPClient::setUserAgent(std::string_view UserAgent) {
  mUserAgent = std::string(UserAgent.cbegin(), UserAgent.cend());
}

void HTTPClient::loadConfig(std::string_view ConfigPath) {
  auto ConfigFile   = toml::parse_file(ConfigPath);
  auto ConfigObject = ConfigFile["HTTPClient"];

  mUserAgent  = ConfigObject["UserAgent"].value_or("");
  mDedeUserID = ConfigObject["DedeUserID"].value_or("");
  mSESSDATA   = ConfigObject["SESSDATA"].value_or("");
  Aria2MaxConnectionPerServer =
      ConfigObject["Aria2MaxConnectionPerServer"].value_or(1);
  Aria2MaxConcurrentDownloads =
      ConfigObject["Aria2MaxConcurrentDownloads"].value_or(5);
  AriaSplit = ConfigObject["Aria2Split"].value_or(5);

  if (mDedeUserID.empty() || mSESSDATA.empty()) {
    mDedeUserID.clear();
    mSESSDATA.clear();
  }
}
} // namespace bilibiliutils