#ifndef BILIBILIUTILS_ASSCONVERTCONTEXT_H
#define BILIBILIUTILS_ASSCONVERTCONTEXT_H

#include "Danmaku.h"

#include <cstdint>
#include <functional>
#include <string>
#include <string_view>
#include <unordered_map>
#include <utility>

namespace bilibiliutils {
class ASSConvertContext;
class ASSConvertContextBuilder;

class ASSConvertContext {
public:
  using DanmakuVelocityFn =
      std::function<double(ASSConvertContext const &, Danmaku const &)>;

  static DanmakuVelocityFn SimpleVelocityFn(unsigned ExpectedTTL);
  static DanmakuVelocityFn FixedTimeVelocityFn(unsigned ExpectedTTL);

private:
  unsigned mCanvasResX = 960;
  unsigned mCanvasResY = 540;

  std::string mTitle;

  std::string mFontName    = "sans-serif";
  unsigned mFontSizeSmall  = 24;
  unsigned mFontSizeMedium = 29;
  unsigned mFontSizeLarge  = 40;

  bool mIsUsingBoldFont      = true;
  bool mIsUsingItalicFont    = false;
  bool mIsUsingUnderlineFont = false;

  std::uint8_t mAlpha = 0x30;

  DanmakuVelocityFn mVelocityFn = SimpleVelocityFn(5000);

  unsigned mFixedDanmakuTTL        = 5000;
  unsigned mFixedDanmakuPaddingTTL = 0;
  unsigned mPaddingTopBottom       = 0; // Only for Rolling Danmakus
  unsigned mPaddingLeftRight       = 5; // Only for Rolling Danmakus

  std::unordered_map<Danmaku::Modes, unsigned> mLayers = {
      {Danmaku::Modes::Normal, 0},
      {Danmaku::Modes::Reverse, 0},
      {Danmaku::Modes::Top, 1},
      {Danmaku::Modes::Bottom, 1},
      {Danmaku::Modes::FX, 2},
      {Danmaku::Modes::Code, 2},
      {Danmaku::Modes::BAS, 2}};

  bool mEnforceNoOverlay = true;
  unsigned mMaxDelay     = 500;

  friend class ASSConvertContextBuilder;

public:
  unsigned getCanvasResX() const { return mCanvasResX; }
  unsigned getCanvasResY() const { return mCanvasResY; }

  std::string_view getTitle() const { return mTitle; }

  std::string_view getFontName() const { return mFontName; }

  unsigned getFontSizeSmall() const { return mFontSizeSmall; }
  unsigned getFontSizeMedium() const { return mFontSizeMedium; }
  unsigned getFontSizeLarge() const { return mFontSizeLarge; }
  unsigned getAdjustedFontSize(Danmaku const &D) const;

  bool isUsingBoldFont() const { return mIsUsingBoldFont; }
  bool isUsingItalicFont() const { return mIsUsingItalicFont; }
  bool isUsingUnderlineFont() const { return mIsUsingUnderlineFont; }

  std::uint8_t getAlpha() const { return mAlpha; }

  double getVelocity(Danmaku const &D) const { return mVelocityFn(*this, D); }

  unsigned getFixedDanmakuTTL() const { return mFixedDanmakuTTL; }
  unsigned getFixedDanmakuPaddingTTL() const { return mFixedDanmakuPaddingTTL; }

  unsigned getPaddingTopBottom() const { return mPaddingTopBottom; }
  unsigned getPaddingLeftRight() const { return mPaddingLeftRight; }

  struct Rectangle : std::pair<unsigned, unsigned> {
    using std::pair<unsigned, unsigned>::pair;
    unsigned width() const { return this->first; }
    unsigned height() const { return this->second; }
  };

  Rectangle getCollisionBoxSize(Danmaku const &D) const;

  unsigned getLayer(Danmaku const &D) const;

  bool isEnforcingNoOverlay() const { return mEnforceNoOverlay; }
  unsigned getMaxDelay() const { return mMaxDelay; }

  void loadConfig(std::string_view ConfigPath);
};

class ASSConvertContextBuilder {
  ASSConvertContext mContext;

public:
  using Builder = ASSConvertContextBuilder;

  Builder &setCanvasResX(unsigned CanvasResX);
  Builder &setCanvasResY(unsigned CanvasResY);

  Builder &setTitle(std::string_view Title);

  Builder &setFontName(std::string_view FontName);
  Builder &setFontSizeSmall(unsigned FontSizeSmall);
  Builder &setFontSizeMedium(unsigned FontSizeMedium);
  Builder &setFontSizeLarge(unsigned FontSizeLarge);

  Builder &useBoldFont(bool UseBoldFont);
  Builder &useItalicFont(bool UseItalicFont);
  Builder &useUnderlineFont(bool UseUnderlineFont);

  Builder &setFontAlpha(std::uint8_t Alpha);

  using DanmakuVelocityFn = ASSConvertContext::DanmakuVelocityFn;
  Builder &setVelocityFunctor(DanmakuVelocityFn Functor);

  Builder &setFixedDanmakuTTL(unsigned FixedDanmakuTTL);
  Builder &setFixedDanmakuPaddingTTL(unsigned FixedDanmakuPaddingTTL);

  Builder &setPaddingTopBottom(unsigned PaddingTopBottom);
  Builder &setPaddingLeftRight(unsigned PaddingLeftRight);

  Builder &setLayer(Danmaku::Modes Mode, unsigned Layer);

  Builder &enforceNoOverlay(bool NoOverlay);
  Builder &setMaxDelay(unsigned MaxDelay);

  ASSConvertContext build();

  Builder &loadConfig(std::string_view ConfigPath);
};
} // namespace bilibiliutils

#endif