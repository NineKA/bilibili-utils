#ifndef BILIBILIUTILS_EXCEPTIONS_H
#define BILIBILIUTILS_EXCEPTIONS_H

#include <stdexcept>
#include <string_view>

namespace bilibiliutils {
class HTTPError : public std::runtime_error {
  std::string mURL;
  long mCode;

public:
  HTTPError(std::string_view URL, long Code)
      : std::runtime_error("HTTP response returns with non-200 code"),
        mURL(std::string(URL.cbegin(), URL.cend())), mCode(Code) {}

  std::string_view getURL() const { return mURL; }
  long getReturnCode() const { return mCode; }
};

class RemoteError : public std::runtime_error {
  int mCode;
  std::string mMessage;

public:
  RemoteError(int Code, std::string_view Message)
      : std::runtime_error("remote error from bilibili.com"), mCode(Code),
        mMessage(Message) {}

  int getErrorCode() const { return mCode; }
  std::string_view getMessage() const { return mMessage; }
};
} // namespace bilibiliutils
#endif
