#ifndef BILIBILIUTILS_HTTPCLIENT_H
#define BILIBILIUTILS_HTTPCLIENT_H

#include <functional>
#include <memory>
#include <string>
#include <string_view>
#include <vector>

#include <aria2/aria2.h>

namespace bilibiliutils {
class HTTPClient {
public:
  class Aria2DownloadTask {
    std::string mURL;
    std::string mPath;

  public:
    Aria2DownloadTask(std::string_view URL, std::string_view Path);
    std::string_view getURL() const { return mURL; }
    std::string_view getPath() const { return mPath; }
  };

private:
  std::string mDedeUserID;
  std::string mSESSDATA;
  std::string mUserAgent;

  unsigned Aria2MaxConnectionPerServer = 1;
  unsigned Aria2MaxConcurrentDownloads = 5;
  unsigned AriaSplit                   = 5;

  std::vector<std::unique_ptr<Aria2DownloadTask>> mTasks;

public:
  std::string get(std::string_view URL) const;

  Aria2DownloadTask *addAria2Task(std::string_view URL, std::string_view Path);

  using CallbackFn =
      std::function<void(Aria2DownloadTask *, aria2::DownloadHandle *)>;
  static void Ignore(Aria2DownloadTask *, aria2::DownloadHandle *);
  void runAria2Tasks(CallbackFn const &Callback = Ignore);

  void setLogin(std::string_view DedeUserID, std::string_view SESSDATA);
  void setUserAgent(std::string_view UserAgent);

  void loadConfig(std::string_view ConfigPath);
};
} // namespace bilibiliutils

#endif
