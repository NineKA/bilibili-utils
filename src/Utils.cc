#include "Utils.h"

#include <fmt/format.h>

#include <iomanip>
#include <regex>
#include <sstream>

namespace bilibiliutils::utils {
std::string formatHumanReadableSpeed(std::size_t BytesPerSecond) {
  char const *Suffix[] = {"B/s", "KiB/s", "MiB/s", "GiB/s", "TiB/s"};
  auto Length          = sizeof(Suffix) / sizeof(Suffix[0]);
  std::size_t I        = 0;
  auto BytesInFP       = static_cast<double>(BytesPerSecond);
  if (BytesPerSecond > 1024) {
    for (I = 0; (BytesPerSecond / 1024) > 0 && I < Length - 1;
         I++, BytesPerSecond /= 1024)
      BytesInFP = static_cast<double>(BytesPerSecond) / 1024.0;
  }
  return fmt::format("{:.02f} {}", BytesInFP, Suffix[I]);
}

unsigned checkAvID(std::string_view AvIDString) {
  std::string OwnedAvIDString(AvIDString);
  unsigned AvID;
  std::regex AvIDRegex("av[0-9]+");
  if (std::regex_match(OwnedAvIDString, AvIDRegex)) {
    OwnedAvIDString = AvIDString.substr(2);
    AvID            = std::stoul(OwnedAvIDString);
    return AvID;
  }
  throw std::runtime_error("unknown AvID format");
}

std::string checkBvID(std::string_view BvIDString) {
  std::regex BvIDRegex("BV[0-9a-zA-Z]+");
  if (std::regex_match(BvIDString.begin(), BvIDString.end(), BvIDRegex)) {
    return std::string(BvIDString);
  }
  throw std::runtime_error("unknown BvID format");
}

std::string formatIndex(unsigned TotalCount, unsigned Index) {
  int Digits = 0;
  while (TotalCount != 0) {
    TotalCount /= 10;
    Digits++;
  }
  std::ostringstream Stream;
  Stream << std::setw(Digits) << std::setfill('0') << Index;
  return Stream.str();
}
} // namespace bilibiliutils::utils