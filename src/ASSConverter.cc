#include "ASSConverter.h"

#include <fmt/format.h>

#include <algorithm>
#include <iterator>
#include <limits>
#include <regex>

namespace {
struct ASSTimePoint {
  unsigned mProgress;
  explicit ASSTimePoint(unsigned Progress) : mProgress(Progress) {}
};
} // namespace

namespace fmt {
template <> struct formatter<ASSTimePoint> {
  template <typename C> auto parse(C &&CTX) { return CTX.begin(); }
  template <typename C> auto format(ASSTimePoint const &TimePoint, C &&CTX) {
    constexpr char const *FmtStr = "{:01d}:{:02d}:{:02d}.{:02d}";
    auto Hour                    = TimePoint.mProgress / (60 * 60 * 1000);
    auto Minute    = (TimePoint.mProgress % (60 * 60 * 1000)) / (60 * 1000);
    auto Second    = (TimePoint.mProgress % (60 * 1000)) / 1000;
    auto Hundredth = (TimePoint.mProgress % 1000) / 10;
    return fmt::format_to(CTX.out(), FmtStr, Hour, Minute, Second, Hundredth);
  }
};
} // namespace fmt

namespace bilibiliutils {
RollingTrackSelector::RollingTrackSelector(ASSConvertContext *Context)
    : mContext(Context) {
  mRecord.resize(mContext->getCanvasResY(), {0, 0});
}

RollingTrackSelector::Result RollingTrackSelector::place(Danmaku const &D) {
  auto [CBWidth, CBHeight] = mContext->getCollisionBoxSize(D);
  auto V                   = mContext->getVelocity(D);
  auto LeftSideEnterTime   = D.getProgress();
  auto LeftSideExitTime =
      D.getProgress() + static_cast<unsigned>(mContext->getCanvasResX() / V);

  auto UpdatedRightSideEnterTime =
      D.getProgress() + static_cast<unsigned>(CBWidth / V);
  auto UpdatedRightSideExitTime =
      D.getProgress() +
      static_cast<unsigned>((mContext->getCanvasResX() + CBWidth) / V);

  for (unsigned I = 0; I < mContext->getCanvasResY() - CBHeight; ++I) {
    unsigned Delay = 0;
    auto Found     = std::all_of(
        mRecord.cbegin() + I,
        mRecord.cbegin() + I + CBHeight,
        [this, LeftSideEnterTime, LeftSideExitTime, &Delay](
            auto const &Record) {
          auto [LastRightSideEnterTime, LastRightSideExitTime] = Record;
          if ((LastRightSideEnterTime <=
               LeftSideEnterTime + mContext->getMaxDelay()) &&
              (LastRightSideExitTime <=
               LeftSideExitTime + mContext->getMaxDelay())) {
            auto DelayEnter = 0;
            auto DelayExit  = 0;
            if (LastRightSideEnterTime > LeftSideEnterTime)
              DelayEnter = LastRightSideEnterTime - LeftSideEnterTime;
            if (LastRightSideExitTime > LeftSideEnterTime)
              DelayExit = LastRightSideExitTime - LeftSideExitTime;
            Delay = std::max(DelayEnter, DelayExit);
            return true;
          }
          return false;
        });
    if (Found) {
      UpdatedRightSideEnterTime = UpdatedRightSideEnterTime + Delay;
      UpdatedRightSideExitTime  = UpdatedRightSideExitTime + Delay;
      std::fill(
          mRecord.begin() + I,
          mRecord.begin() + I + CBHeight,
          std::make_pair(UpdatedRightSideEnterTime, UpdatedRightSideExitTime));
      return {true, I, I + CBHeight, Delay};
    }
  }

  if (mContext->isEnforcingNoOverlay()) return {false, 0, 0, 0};

  auto MinError = std::numeric_limits<unsigned>::max();
  unsigned Pos  = 0;

  for (unsigned I = 0; I < mContext->getCanvasResY() - CBHeight; ++I) {
    unsigned Error = 0;
    for (unsigned J = 0; J < CBHeight; ++J) {
      auto [LastRightSideEnterTime, LastRightSideExitTime] = mRecord[I + J];
      if (LastRightSideEnterTime > LeftSideEnterTime)
        Error += LastRightSideEnterTime - LeftSideEnterTime;
      if (LastRightSideExitTime > LeftSideExitTime)
        Error += LastRightSideExitTime - LeftSideExitTime;
    }
    if (Error < MinError) {
      MinError = Error;
      Pos      = I;
    }
  }

  return {false, Pos, Pos + CBHeight, 0};
}

void RollingTrackSelector::reset() {
  std::fill(mRecord.begin(), mRecord.end(), std::make_pair(0, 0));
}

FixedTrackSelector::FixedTrackSelector(ASSConvertContext *Context)
    : mContext(Context) {
  mRecord.resize(Context->getCanvasResY(), 0);
}

FixedTrackSelector::Result FixedTrackSelector::place(Danmaku const &D) {
  auto [CBWidth, CBHeight] = mContext->getCollisionBoxSize(D);
  auto EnterTime           = D.getProgress();
  auto UpdateExitTime      = D.getProgress() + mContext->getFixedDanmakuTTL() +
                        mContext->getFixedDanmakuPaddingTTL();

  for (unsigned I = 0; I < mContext->getCanvasResY() - CBHeight; ++I) {
    unsigned Delay = 0;
    auto Found     = std::all_of(
        mRecord.cbegin() + I,
        mRecord.cbegin() + I + CBHeight,
        [this, EnterTime, &Delay](unsigned ExitTime) {
          if (ExitTime <= EnterTime) {
            return true;
          } else if (ExitTime <= EnterTime + mContext->getMaxDelay()) {
            Delay = ExitTime - EnterTime;
            return true;
          } else {
            return false;
          }
        });
    if (Found) {
      std::fill(
          mRecord.begin() + I,
          mRecord.begin() + I + CBHeight,
          UpdateExitTime + Delay);
      return {true, I, I + CBHeight, Delay};
    }
  }

  if (mContext->isEnforcingNoOverlay()) return {false, 0, 0, 0};

  auto MinError = std::numeric_limits<unsigned>::max();
  unsigned Pos  = 0;

  for (unsigned I = 0; I < mContext->getCanvasResY() - CBHeight; ++I) {
    unsigned Error = 0;
    for (unsigned J = 0; J < CBHeight; ++J) {
      auto LastExitTime = mRecord[I + J];
      if (LastExitTime > EnterTime) Error += LastExitTime - EnterTime;
    }
    if (Error < MinError) {
      MinError = Error;
      Pos      = I;
    }
  }

  return {false, Pos, Pos + CBHeight, 0};
}

void FixedTrackSelector::reset() {
  std::fill(mRecord.begin(), mRecord.end(), 0);
}

ASSConverter::ASSConverter(ASSConvertContext *Context)
    : mContext(Context), mNormalSelector(Context), mReverseSelector(Context),
      mTopSelector(Context), mBottomSelector(Context) {}

std::string ASSConverter::getColorTag(Danmaku const &D) {
  auto Color = D.getColor();
  if (Color == Danmaku::Color::BLACK) return R"(\c&000000&\3c&FFFFFF&)";
  if (Color != Danmaku::Color::WHITE)
    return fmt::format(
        R"(\c&H{:02X}{:02X}{:02X}&)",
        Color.getBlue(),
        Color.getGreen(),
        Color.getRed());
  return {};
}

std::string ASSConverter::getContent(Danmaku const &D) {
  auto UnescapedContent = D.getContent();
  std::string Content(UnescapedContent);
  Content = std::regex_replace(Content, std::regex("\n"), "\\N");
  return Content;
}

std::string_view ASSConverter::getBaselineStyleName(Danmaku const &D) {
  switch (D.getFontSize()) {
  case 18: return "Small";
  case 25: return "Medium";
  case 36: return "Large";
  default: return "Medium"; // unknown fontsize default to medium;
  }
}

bool ASSConverter::addNormal(Danmaku const &D) {
  auto TrackSelectorResult = mNormalSelector.place(D);
  if (mContext->isEnforcingNoOverlay() && !TrackSelectorResult.no_overlay())
    return false;

  auto [CBWidth, CBHeight] = mContext->getCollisionBoxSize(D);
  auto Velocity            = mContext->getVelocity(D);
  auto StartTime           = D.getProgress() + TrackSelectorResult.delay();
  auto EndTime =
      D.getProgress() + TrackSelectorResult.delay() +
      static_cast<unsigned>((mContext->getCanvasResX() + CBWidth) / Velocity);

  auto XStart = mContext->getCanvasResX() + CBWidth / 2;
  auto Y      = (TrackSelectorResult.top() + CBHeight / 2);
  auto XEnd   = CBWidth / 2;

  auto Line = fmt::format(
      "Dialogue: {},{},{},{},,0,0,0,,{{\\move({},{},-{},{}){}}}{}",
      mContext->getLayer(D),
      ASSTimePoint{StartTime},
      ASSTimePoint{EndTime},
      getBaselineStyleName(D),
      XStart,
      Y,
      XEnd,
      Y,
      getColorTag(D),
      getContent(D));

  mDialogs.emplace_back(StartTime, EndTime, std::move(Line));
  return true;
}

bool ASSConverter::addReverse(Danmaku const &D) {
  auto TrackSelectorResult = mReverseSelector.place(D);
  if (mContext->isEnforcingNoOverlay() && !TrackSelectorResult.no_overlay())
    return false;

  auto [CBWidth, CBHeight] = mContext->getCollisionBoxSize(D);
  auto Velocity            = mContext->getVelocity(D);
  auto StartTime           = D.getProgress() + TrackSelectorResult.delay();
  auto EndTime =
      D.getProgress() + TrackSelectorResult.delay() +
      static_cast<unsigned>((mContext->getCanvasResX() + CBWidth) / Velocity);

  auto XStart = CBWidth / 2;
  auto Y      = (TrackSelectorResult.top() + CBHeight / 2);
  auto XEnd   = mContext->getCanvasResX() + CBWidth / 2;

  auto Line = fmt::format(
      "Dialogue: {},{},{},{},,0,0,0,,{{\\move(-{},{},{},{}){}}}{}",
      mContext->getLayer(D),
      ASSTimePoint{StartTime},
      ASSTimePoint{EndTime},
      getBaselineStyleName(D),
      XStart,
      Y,
      XEnd,
      Y,
      getColorTag(D),
      getContent(D));

  mDialogs.emplace_back(StartTime, EndTime, std::move(Line));
  return true;
}

bool ASSConverter::addTop(Danmaku const &D) {
  auto TrackSelectorResult = mTopSelector.place(D);
  if (mContext->isEnforcingNoOverlay() && !TrackSelectorResult.no_overlay())
    return false;

  auto [CBWidth, CBHeight] = mContext->getCollisionBoxSize(D);
  auto StartTime           = D.getProgress() + TrackSelectorResult.delay();
  auto EndTime             = D.getProgress() + mContext->getFixedDanmakuTTL() +
                 TrackSelectorResult.delay();

  auto X = mContext->getCanvasResX() / 2;
  auto Y = TrackSelectorResult.top() + CBHeight / 2;

  auto Line = fmt::format(
      "Dialogue: {},{},{},{},,0,0,0,,{{\\pos({},{}){}}}{}",
      mContext->getLayer(D),
      ASSTimePoint{StartTime},
      ASSTimePoint{EndTime},
      getBaselineStyleName(D),
      X,
      Y,
      getColorTag(D),
      getContent(D));

  mDialogs.emplace_back(StartTime, EndTime, std::move(Line));
  return true;
}

bool ASSConverter::addBottom(const Danmaku &D) {
  auto TrackSelectorResult = mBottomSelector.place(D);
  if (mContext->isEnforcingNoOverlay() && !TrackSelectorResult.no_overlay())
    return false;

  auto [CBWidth, CBHeight] = mContext->getCollisionBoxSize(D);
  auto StartTime           = D.getProgress() + TrackSelectorResult.delay();
  auto EndTime             = D.getProgress() + mContext->getFixedDanmakuTTL() +
                 TrackSelectorResult.delay();

  auto X = mContext->getCanvasResX() / 2;
  auto Y = TrackSelectorResult.top() + CBHeight / 2;

  auto Line = fmt::format(
      "Dialogue: {},{},{},{},,0,0,0,,{{\\pos({},{}){}}}{}",
      mContext->getLayer(D),
      ASSTimePoint{StartTime},
      ASSTimePoint{EndTime},
      getBaselineStyleName(D),
      X,
      mContext->getCanvasResY() - Y,
      getColorTag(D),
      getContent(D));

  mDialogs.emplace_back(StartTime, EndTime, std::move(Line));
  return true;
}

bool ASSConverter::push_back(Danmaku const &D) {
  switch (D.getMode()) {
  case Danmaku::Modes::Normal: return addNormal(D);
  case Danmaku::Modes::Reverse: return addReverse(D);
  case Danmaku::Modes::Top: return addTop(D);
  case Danmaku::Modes::Bottom: return addBottom(D);
  case Danmaku::Modes::FX: [[fallthrough]];
  case Danmaku::Modes::Code: [[fallthrough]];
  case Danmaku::Modes::BAS: return false;
  default: __builtin_trap();
  }
}

void ASSConverter::emitASSHeader(std::ostream &OutputStream) const {
  // clang-format off
  fmt::format_to(std::ostream_iterator<char>(OutputStream),
      #include "ASSHeader.template"
      , fmt::arg("Title", mContext->getTitle())
      , fmt::arg("CanvasResX", mContext->getCanvasResX())
      , fmt::arg("CanvasResY", mContext->getCanvasResY())
      , fmt::arg("FontName", mContext->getFontName())
      , fmt::arg("FontSizeSmall", mContext->getFontSizeSmall())
      , fmt::arg("FontSizeMedium", mContext->getFontSizeMedium())
      , fmt::arg("FontSizeLarge", mContext->getFontSizeLarge())
      , fmt::arg("Alpha", mContext->getAlpha())
      , fmt::arg("Bold", mContext->isUsingBoldFont()?-1:0)
      , fmt::arg("Italic", mContext->isUsingItalicFont()?-1:0)
      , fmt::arg("Underline", mContext->isUsingUnderlineFont()?-1:0)
  );
  // clang-format on
}

void ASSConverter::write(std::ostream &OutputStream) {
  emitASSHeader(OutputStream);
  std::sort(
      mDialogs.begin(),
      mDialogs.end(),
      [](ASSDialog const &LHS, ASSDialog const &RHS) {
        return LHS.getStartTimeMS() < RHS.getStartTimeMS();
      });
  for (auto &&Dialog : mDialogs) OutputStream << Dialog.getLine() << '\n';
}
} // namespace bilibiliutils