#ifndef BILIBILIUTILS_ASSCONVERTER_H
#define BILIBILIUTILS_ASSCONVERTER_H

#include "ASSConvertContext.h"
#include "Danmaku.h"

#include <ostream>
#include <range/v3/range/concepts.hpp>
#include <string_view>
#include <utility>

namespace bilibiliutils {
class RollingTrackSelector {
  ASSConvertContext *mContext;
  // <Right-side Enter Time, Right-side Exit Time>
  std::vector<std::pair<unsigned, unsigned>> mRecord;

public:
  explicit RollingTrackSelector(ASSConvertContext *Context);

  struct Result : std::tuple<bool, unsigned, unsigned, unsigned> {
    using std::tuple<bool, unsigned, unsigned, unsigned>::tuple;
    bool no_overlay() { return std::get<0>(*this); }
    unsigned top() { return std::get<1>(*this); }
    unsigned bottom() { return std::get<2>(*this); }
    unsigned delay() { return std::get<3>(*this); }
  };

  Result place(Danmaku const &D);
  void reset();
};

class FixedTrackSelector {
  ASSConvertContext *mContext;
  std::vector<unsigned> mRecord;

public:
  explicit FixedTrackSelector(ASSConvertContext *Context);

  struct Result : std::tuple<bool, unsigned, unsigned, unsigned> {
    using std::tuple<bool, unsigned, unsigned, unsigned>::tuple;
    bool no_overlay() { return std::get<0>(*this); }
    unsigned top() { return std::get<1>(*this); }
    unsigned bottom() { return std::get<2>(*this); }
    unsigned delay() { return std::get<3>(*this); }
  };

  Result place(Danmaku const &D);
  void reset();
};

class ASSDialog {
  unsigned mStartTimeMS;
  unsigned mEndTimeMS;
  std::string mLine;

public:
  ASSDialog(unsigned StartTimeMS, unsigned EndTimeMS, std::string Line)
      : mStartTimeMS(StartTimeMS), mEndTimeMS(EndTimeMS),
        mLine(std::move(Line)) {}

  unsigned getStartTimeMS() const { return mStartTimeMS; }
  unsigned getEndTimeMS() const { return mEndTimeMS; }
  std::string_view getLine() const { return mLine; }
};

class ASSConverter {
  ASSConvertContext *mContext;
  std::vector<ASSDialog> mDialogs;

  RollingTrackSelector mNormalSelector;
  RollingTrackSelector mReverseSelector;
  FixedTrackSelector mTopSelector;
  FixedTrackSelector mBottomSelector;

  void emitASSHeader(std::ostream &OutputStream) const;

  static std::string getColorTag(Danmaku const &D);
  static std::string getContent(Danmaku const &D);
  static std::string_view getBaselineStyleName(Danmaku const &D);

  bool addNormal(Danmaku const &D);
  bool addReverse(Danmaku const &D);
  bool addTop(Danmaku const &D);
  bool addBottom(Danmaku const &D);

public:
  explicit ASSConverter(ASSConvertContext *Context);

  bool push_back(Danmaku const &D);

  template <typename Iterator>
  std::size_t push_back(Iterator Begin, Iterator End) {
    std::size_t SuccessCount = 0;
    for (auto I = Begin; I != End; ++I)
      if (push_back(*I)) SuccessCount = SuccessCount + 1;
    return SuccessCount;
  }

  void write(std::ostream &OutputStream);

  ASSConvertContext *getContext() const { return mContext; }
};
} // namespace bilibiliutils

#endif