#include "ASSConvertContext.h"

#include <toml++/toml.h>

#include <regex>
#include <type_traits>

namespace bilibiliutils {
ASSConvertContext::DanmakuVelocityFn
ASSConvertContext::SimpleVelocityFn(unsigned ExpectedTTL) {
  return [=](ASSConvertContext const &Context, Danmaku const &D) -> double {
    auto TTL      = Context.getCanvasResX() / 524.0 * ExpectedTTL;
    auto FontSize = Context.getAdjustedFontSize(D);
    unsigned MoveDistance;
    if (D.getWidth() <= 12) {
      MoveDistance = Context.getCanvasResX() + 12 * FontSize +
                     Context.getPaddingLeftRight() * 2;
    } else if (D.getWidth() <= 15) {
      MoveDistance = Context.getCanvasResX() + 15 * FontSize +
                     Context.getPaddingLeftRight() * 2;
    } else {
      MoveDistance =
          Context.getCanvasResX() + Context.getCollisionBoxSize(D).width();
    }
    return static_cast<double>(MoveDistance) / TTL;
  };
}

ASSConvertContext::DanmakuVelocityFn
ASSConvertContext::FixedTimeVelocityFn(unsigned ExpectedTTL) {
  return [=](ASSConvertContext const &Context, Danmaku const &D) -> double {
    auto [CBWidth, CBHeight] = Context.getCollisionBoxSize(D);
    auto TTL                 = Context.getCanvasResX() / 524.0 * ExpectedTTL;
    auto MoveDistance        = Context.getCanvasResX() + CBWidth;
    return static_cast<double>(MoveDistance) / TTL;
  };
}

unsigned ASSConvertContext::getAdjustedFontSize(Danmaku const &D) const {
  switch (D.getFontSize()) {
  case 18: return mFontSizeSmall;
  case 25: return mFontSizeMedium;
  case 36: return mFontSizeLarge;
  default: return mFontSizeMedium; // unknown fontsize default to medium;
  }
}

ASSConvertContext::Rectangle
ASSConvertContext::getCollisionBoxSize(Danmaku const &D) const {
  auto AdjustedFontSize = getAdjustedFontSize(D);
  auto Width  = D.getWidth() * AdjustedFontSize + mPaddingLeftRight * 2;
  auto Height = D.getHeight() * AdjustedFontSize + mPaddingTopBottom * 2;
  return {Width, Height};
}

unsigned ASSConvertContext::getLayer(Danmaku const &D) const {
  return mLayers.at(D.getMode());
}

void ASSConvertContext::loadConfig(std::string_view ConfigPath) {
  auto ConfigFile   = toml::parse_file(ConfigPath);
  auto ConfigObject = ConfigFile["ASSConvert"];

  mCanvasResX = ConfigObject["CanvasX"].value_or(960);
  mCanvasResY = ConfigObject["CanvasY"].value_or(540);

  std::string VelocityFnString =
      ConfigObject["Velocity"].value_or("SimpleVelocityFn(5000)");
  std::regex SVFnRegex(R"(SimpleVelocityFn\(([0-9]+)\))");
  std::regex FTFnRegex(R"(FixedTimeVelocityFn\((0-9]+)\))");
  std::smatch RegexResult;
  if (!std::regex_match(VelocityFnString, RegexResult, SVFnRegex) &&
      !std::regex_match(VelocityFnString, RegexResult, FTFnRegex)) {
    throw std::runtime_error("unknown velocity function");
  }
  mVelocityFn = SimpleVelocityFn(std::stoul(RegexResult[1]));

  mFixedDanmakuTTL        = ConfigObject["FixedDanmakuTTL"].value_or(5000);
  mFixedDanmakuPaddingTTL = ConfigObject["FixedDanmakuPaddingTTL"].value_or(0);
  mPaddingTopBottom       = ConfigObject["PaddingTopBottom"].value_or(0);
  mPaddingLeftRight       = ConfigObject["PaddingLeftRight"].value_or(5);
  mEnforceNoOverlay       = ConfigObject["EnforceNoOverlay"].value_or(true);
  mMaxDelay               = ConfigObject["MaxDelay"].value_or(500);

  auto FontConfigObject = ConfigObject["Font"];
  mFontName             = FontConfigObject["Name"].value_or("sans-serif");
  mFontSizeSmall        = FontConfigObject["SizeSmall"].value_or(24);
  mFontSizeMedium       = FontConfigObject["SizeMedium"].value_or(29);
  mFontSizeLarge        = FontConfigObject["SizeLarge"].value_or(40);
  mIsUsingBoldFont      = FontConfigObject["UseBold"].value_or(true);
  mIsUsingItalicFont    = FontConfigObject["UseItalic"].value_or(false);
  mIsUsingUnderlineFont = FontConfigObject["UseUnderline"].value_or(false);
  mAlpha                = FontConfigObject["Alpha"].value_or(0x30);

  auto LayerConfigObject           = ConfigObject["Layer"];
  mLayers[Danmaku::Modes::Normal]  = LayerConfigObject["Normal"].value_or(0);
  mLayers[Danmaku::Modes::Reverse] = LayerConfigObject["Reverse"].value_or(0);
  mLayers[Danmaku::Modes::Top]     = LayerConfigObject["Top"].value_or(1);
  mLayers[Danmaku::Modes::Bottom]  = LayerConfigObject["Bottom"].value_or(1);
  mLayers[Danmaku::Modes::FX]      = LayerConfigObject["FX"].value_or(2);
  mLayers[Danmaku::Modes::Code]    = LayerConfigObject["Code"].value_or(2);
  mLayers[Danmaku::Modes::BAS]     = LayerConfigObject["BAS"].value_or(2);
}

using Builder = ASSConvertContextBuilder;

#define SETTER(name, arg_type, field)                                          \
  Builder &Builder::name(arg_type arg) {                                       \
    if constexpr (                                                             \
        !std::is_trivially_copy_assignable_v<arg_type> &&                      \
        std::is_move_assignable_v<arg_type>) {                                 \
      mContext.field = std::move(arg);                                         \
    } else {                                                                   \
      mContext.field = arg;                                                    \
    }                                                                          \
    return *this;                                                              \
  }

SETTER(setCanvasResX, unsigned, mCanvasResX)
SETTER(setCanvasResY, unsigned, mCanvasResY)

SETTER(setTitle, std::string_view, mTitle)

SETTER(setFontName, std::string_view, mFontName)
SETTER(setFontSizeSmall, unsigned, mFontSizeSmall)
SETTER(setFontSizeMedium, unsigned, mFontSizeMedium)
SETTER(setFontSizeLarge, unsigned, mFontSizeLarge)

SETTER(useBoldFont, bool, mIsUsingBoldFont)
SETTER(useItalicFont, bool, mIsUsingItalicFont)
SETTER(useUnderlineFont, bool, mIsUsingUnderlineFont)

SETTER(setFontAlpha, std::uint8_t, mAlpha)

SETTER(setVelocityFunctor, DanmakuVelocityFn, mVelocityFn)

SETTER(setFixedDanmakuTTL, unsigned, mFixedDanmakuTTL)
SETTER(setFixedDanmakuPaddingTTL, unsigned, mFixedDanmakuPaddingTTL)

SETTER(setPaddingTopBottom, unsigned, mPaddingLeftRight)
SETTER(setPaddingLeftRight, unsigned, mPaddingTopBottom)

SETTER(enforceNoOverlay, bool, mEnforceNoOverlay)
SETTER(setMaxDelay, unsigned, mMaxDelay)

#undef SETTER

Builder &Builder::setLayer(Danmaku::Modes Mode, unsigned Layer) {
  mContext.mLayers[Mode] = Layer;
  return *this;
}

ASSConvertContext Builder::build() { return mContext; }

Builder &Builder::loadConfig(std::string_view ConfigPath) {
  mContext.loadConfig(ConfigPath);
  return *this;
}
} // namespace bilibiliutils