#include "Video.h"

#include <utf8.h>

#include <algorithm>
#include <concepts>
#include <regex>
#include <stdexcept>
#include <utility>

#include "Exceptions.h"

namespace bilibiliutils {
namespace {
std::string verifyUTF8Str(nlohmann::json const &StringNode) {
  auto String     = StringNode.get<std::string_view>();
  auto SearchIter = utf8::find_invalid(String.cbegin(), String.cend());
  if (SearchIter != String.cend()) {
    auto Pos = utf8::distance(String.cbegin(), SearchIter);
    auto Message =
        fmt::format("{} contains invalid utf8 character at {}", String, Pos);
    throw std::runtime_error(Message);
  }
  return {String.cbegin(), String.cend()};
}
} // namespace

Video::Video(HTTPClient *Client, std::string_view Response)
    : mHTTPClient(Client) {
  auto ResponseObject = nlohmann::json::parse(Response);

  if (ResponseObject["code"].get<int>() != 0) {
    auto Code    = ResponseObject["code"].get<int>();
    auto Message = verifyUTF8Str(ResponseObject["message"]);
    throw RemoteError(Code, Message);
  }

  auto DataObject = ResponseObject["data"];
  mAvID           = DataObject["aid"].get<unsigned>();
  mBvID           = verifyUTF8Str(DataObject["bvid"]);
  mTitle          = verifyUTF8Str(DataObject["title"]);
  mDescription    = verifyUTF8Str(DataObject["desc"]);
  mCoverPicURL    = verifyUTF8Str(DataObject["pic"]);
  mPublishTime    = DataObject["pubdate"].get<std::time_t>();
  mSubmitTime     = DataObject["ctime"].get<std::time_t>();

  auto OwnerObject = DataObject["owner"];
  mOwnerMID        = OwnerObject["mid"].get<unsigned>();
  mOwnerName       = verifyUTF8Str(OwnerObject["name"]);
  mOwnerPicURL     = verifyUTF8Str(OwnerObject["face"]);

  for (auto &&PageObject : DataObject["pages"]) {
    VideoPart Page(mHTTPClient, mBvID, PageObject);
    mParts.push_back(std::move(Page));
  }
}

Video Video::fromAvID(HTTPClient *Client, unsigned AvID) {
  // clang-format off
  auto URL = fmt::format(
      "https://api.bilibili.com/x/web-interface/view?aid={}",
      AvID);
  // clang-format on
  auto Response = Client->get(URL);
  return Video(Client, Response);
}

Video Video::fromBvID(HTTPClient *Client, std::string_view BvID) {
  // clang-format off
  auto URL = fmt::format(
      "https://api.bilibili.com/x/web-interface/view?bvid={}",
      BvID);
  // clang-format on
  auto Response = Client->get(URL);
  return Video(Client, Response);
}

Video Video::fromURL(HTTPClient *Client, std::string_view URL) {
  std::string OwnedURL(URL.cbegin(), URL.cend());
  auto BVRegex = std::regex("bilibili.com/video/BV([0-9a-zA-Z]+)");
  auto AVRegex = std::regex("bilibili.com/video/av([0-9]+)");
  std::smatch RegexMatches;
  if (std::regex_search(OwnedURL, RegexMatches, AVRegex)) {
    return fromAvID(Client, std::stoul(RegexMatches[1].str()));
  } else if (std::regex_search(OwnedURL, RegexMatches, BVRegex)) {
    return fromBvID(Client, RegexMatches[1].str());
  } else {
    throw std::invalid_argument("unknown bilibili.com URL");
  }
}

VideoPart::VideoPart(
    HTTPClient *HTTPClient, std::string_view BvID,
    nlohmann::json const &PageObject)
    : mHTTPClient(HTTPClient), mBvID(BvID.cbegin(), BvID.cend()),
      mVideoStreams(nullptr), mAudioStreams(nullptr), mDanmakus(nullptr) {
  mIndex    = PageObject["page"].get<unsigned>();
  mCID      = PageObject["cid"].get<unsigned>();
  mTitle    = verifyUTF8Str(PageObject["part"]);
  mDuration = PageObject["duration"].get<unsigned>();
}

unsigned VideoPart::getFnValFlag() const {
  unsigned FnValFlag = 16;
  // clang-format off
  if (mEnableHDR        ) FnValFlag |=   64;
  if (mEnable4K         ) FnValFlag |=  128;
  if (mEnableDolbyAudio ) FnValFlag |=  256;
  if (mEnableDolbyVision) FnValFlag |=  512;
  if (mEnable8K         ) FnValFlag |= 1024;
  if (mEnableAV1Codec   ) FnValFlag |= 2048;
  // clang-format on
  return FnValFlag;
}

unsigned VideoPart::getDanmakuSegmentCount() const {
  unsigned const SIX_MINUTES_IN_SECONDS = 6 * 60;
  return mDuration / SIX_MINUTES_IN_SECONDS +
         ((mDuration % SIX_MINUTES_IN_SECONDS == 0) ? 0 : 1);
}

void VideoPart::refreshVideoAudioStreamInfo() {
  auto URL = fmt::format(
      "https://api.bilibili.com/x/player/playurl?"
      "bvid={}&cid={}&fnval={}&fnver={}&fourk={}",
      mBvID,
      mCID,
      getFnValFlag(),
      0,
      (mEnable4K) ? 1 : 0);

  auto Response       = mHTTPClient->get(URL);
  auto ResponseObject = nlohmann::json::parse(Response);

  if (ResponseObject["code"].get<int>() != 0) {
    auto Code    = ResponseObject["code"].get<int>();
    auto Message = verifyUTF8Str(ResponseObject["message"]);
    throw RemoteError(Code, Message);
  }

  auto DataObject = ResponseObject["data"];
  auto DashObject = DataObject["dash"];

  if (mVideoStreams == nullptr) {
    mVideoStreams = std::make_shared<std::vector<VideoStream>>();
  } else {
    mVideoStreams->clear();
  }

  for (auto &&VideoObject : DashObject["video"]) {
    VideoStream Stream(mHTTPClient, VideoObject);
    mVideoStreams->push_back(std::move(Stream));
  }

  std::sort(
      mVideoStreams->begin(),
      mVideoStreams->end(),
      [](VideoStream const &LHS, VideoStream const &RHS) {
        auto LHSQuality = static_cast<unsigned>(LHS.getQuality());
        auto RHSQuality = static_cast<unsigned>(RHS.getQuality());
        return LHSQuality > RHSQuality;
      });

  if (mAudioStreams == nullptr) {
    mAudioStreams = std::make_shared<std::vector<AudioStream>>();
  } else {
    mAudioStreams->clear();
  }

  for (auto &&AudioObject : DashObject["audio"]) {
    AudioStream Stream(mHTTPClient, AudioObject);
    mAudioStreams->push_back(std::move(Stream));
  }

  std::sort(
      mAudioStreams->begin(),
      mAudioStreams->end(),
      [](AudioStream const &LHS, AudioStream const &RHS) {
        auto LHSQuality = static_cast<unsigned>(LHS.getQuality());
        auto RHSQuality = static_cast<unsigned>(RHS.getQuality());
        return LHSQuality > RHSQuality;
      });
}

void VideoPart::refreshDanmakus() {
  mDanmakus = std::make_shared<std::vector<Danmaku>>();

  auto const SegmentCount = getDanmakuSegmentCount();
  for (unsigned I = 0; I < SegmentCount; ++I) {
    using DmSegMobileReply =
        bilibili::community::service::dm::v1::DmSegMobileReply;
    auto URL = fmt::format(
        "https://api.bilibili.com/x/v2/dm/web/seg.so?"
        "type={}&oid={}&segment_index={}",
        1,
        mCID,
        I + 1);
    auto ReplyObjectBuffer = mHTTPClient->get(URL);
    DmSegMobileReply ReplyObject;
    if (!ReplyObject.ParseFromString(ReplyObjectBuffer)) {
      throw std::runtime_error("malformed reply protobuf response");
    }

    for (auto &&ElemObject : ReplyObject.elems()) {
      mDanmakus->emplace_back(ElemObject);
    }
  }

  std::sort(
      mDanmakus->begin(),
      mDanmakus->end(),
      [](Danmaku const &LHS, Danmaku const &RHS) {
        return LHS.getProgress() < RHS.getProgress();
      });
}

VideoStreamQuality parseVideoStreamQualityCode(unsigned Code) {
  // clang-format off
  switch (Code) {
  case   6: return VideoStreamQuality::VIDEO_240P;
  case  16: return VideoStreamQuality::VIDEO_360P;
  case  32: return VideoStreamQuality::VIDEO_480P;
  case  64: return VideoStreamQuality::VIDEO_720P;
  case  74: return VideoStreamQuality::VIDEO_720P60;
  case  80: return VideoStreamQuality::VIDEO_1080P;
  case 112: return VideoStreamQuality::VIDEO_1080P_PLUS;
  case 116: return VideoStreamQuality::VIDEO_1080P60;
  case 120: return VideoStreamQuality::VIDEO_4K;
  case 125: return VideoStreamQuality::VIDEO_HDR;
  case 126: return VideoStreamQuality::VIDEO_DOLBY;
  case 127: return VideoStreamQuality::VIDEO_8K;
  default : throw std::runtime_error("unknown video stream quality code");
  }
  // clang-format on
}

AudioStreamQuality parseAudioStreamQualityCode(unsigned Code) {
  switch (Code) {
  case 30216: return AudioStreamQuality::AUDIO_64K;
  case 30232: return AudioStreamQuality::AUDIO_132K;
  case 30280: return AudioStreamQuality::AUDIO_192K;
  default: throw std::runtime_error("unknown audio stream quality code");
  }
}

VideoStream::VideoStream(
    HTTPClient *HTTPClient, nlohmann::json const &VideoObject)
    : mHTTPClient(HTTPClient) {
  mQuality   = parseVideoStreamQualityCode(VideoObject["id"].get<unsigned>());
  mURL       = verifyUTF8Str(VideoObject["baseUrl"]);
  mMIMEType  = verifyUTF8Str(VideoObject["mimeType"]);
  mCodec     = verifyUTF8Str(VideoObject["codecs"]);
  mWidth     = VideoObject["width"].get<unsigned>();
  mHeight    = VideoObject["height"].get<unsigned>();
  mFrameRate = VideoObject["frameRate"].get<std::string>();
}

namespace {
bool isStringStartWith(std::string_view String, std::string_view Prefix) {
  if (String.length() < Prefix.length()) return false;
  for (std::size_t I = 0; I < Prefix.length(); ++I)
    if (String[I] != Prefix[I]) return false;
  return true;
}
} // namespace

bool VideoStream::isH264() const { return isStringStartWith(mCodec, "avc1"); }
bool VideoStream::isH265() const { return isStringStartWith(mCodec, "hev1"); }
bool VideoStream::isAV1() const { return isStringStartWith(mCodec, "av01"); }

AudioStream::AudioStream(
    HTTPClient *HTTPClient, nlohmann::json const &AudioObject)
    : mHTTPClient(HTTPClient) {
  mQuality  = parseAudioStreamQualityCode(AudioObject["id"].get<unsigned>());
  mURL      = verifyUTF8Str(AudioObject["baseUrl"]);
  mMIMEType = verifyUTF8Str(AudioObject["mimeType"]);
  mCodec    = verifyUTF8Str(AudioObject["codecs"]);
}
} // namespace bilibiliutils