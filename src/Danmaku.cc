#include "Danmaku.h"

#include <fmt/format.h>
#include <utf8.h>

#include <regex>

namespace bilibiliutils {
namespace {
std::string verifyUTF8Str(std::string_view const &String) {
  auto SearchIter = utf8::find_invalid(String.cbegin(), String.cend());
  if (SearchIter != String.cend()) {
    auto Pos = utf8::distance(String.cbegin(), SearchIter);
    auto Message =
        fmt::format("{} contains invalid utf8 character at {}", String, Pos);
    throw std::runtime_error(Message);
  }
  return {String.cbegin(), String.cend()};
}
} // namespace

Danmaku::Color Danmaku::Color::fromRGB888(std::uint32_t Code) {
  auto R = static_cast<uint8_t>((Code & 0xFF0000) >> 16);
  auto G = static_cast<uint8_t>((Code & 0x00FF00) >> 8);
  auto B = static_cast<uint8_t>(Code & 0x0000FF);
  return {R, G, B};
}

Danmaku::Color Danmaku::Color::WHITE = Danmaku::Color(255, 255, 255);
Danmaku::Color Danmaku::Color::BLACK = Danmaku::Color(0, 0, 0);

Danmaku::Danmaku(ProtoBufObject const &ProtoBufElem) {
  mID       = ProtoBufElem.id();
  mProgress = ProtoBufElem.progress();

  switch (ProtoBufElem.mode()) {
  case 1: [[fallthrough]];
  case 2: [[fallthrough]];
  case 3: mMode = Modes::Normal; break;
  case 4: mMode = Modes::Bottom; break;
  case 5: mMode = Modes::Top; break;
  case 6: mMode = Modes::Reverse; break;
  case 7: mMode = Modes::FX; break;
  case 8: mMode = Modes::Code; break;
  case 9: mMode = Modes::BAS; break;
  default: throw std::invalid_argument("unknown danmaku mode");
  }

  mFontSize   = ProtoBufElem.fontsize();
  mColor      = Color::fromRGB888(ProtoBufElem.color());
  mMidHash    = verifyUTF8Str(ProtoBufElem.midhash());
  mContent    = verifyUTF8Str(ProtoBufElem.content());
  mSubmitTime = static_cast<std::time_t>(ProtoBufElem.ctime());
  mWeight     = ProtoBufElem.weight();

  mContent = std::regex_replace(mContent, std::regex(R"(\\n|\\N)"), "\n");

  switch (ProtoBufElem.pool()) {
  case 0: mPool = Pools::Normal; break;
  case 1: mPool = Pools::Subtitle; break;
  case 2: mPool = Pools::Special; break;
  default: throw std::invalid_argument("unknown danmaku pool");
  }
}

unsigned Danmaku::getWidth() const {
  auto Content      = getContent();
  unsigned MaxWidth = 0;
  auto PreviousPos  = Content.cbegin();
  for (auto Iter = Content.cbegin(); Iter != Content.cend(); ++Iter) {
    if (*Iter != '\n') continue;
    auto Width  = utf8::distance(PreviousPos, Iter);
    MaxWidth    = (MaxWidth < Width) ? Width : MaxWidth;
    PreviousPos = std::next(Iter);
  }
  auto FinalWidth = utf8::distance(PreviousPos, Content.cend());
  MaxWidth        = (MaxWidth < FinalWidth) ? FinalWidth : MaxWidth;
  return MaxWidth;
}

unsigned Danmaku::getHeight() const {
  auto Content      = getContent();
  auto NumLineBreak = std::count(Content.begin(), Content.end(), '\n');
  return NumLineBreak + 1;
}
} // namespace bilibiliutils