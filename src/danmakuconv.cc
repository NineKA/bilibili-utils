#include <cxxopts.hpp>
#include <fmt/format.h>

#include "ASSConvertContext.h"
#include "ASSConverter.h"
#include "HTTPClient.h"
#include "Utils.h"
#include "Video.h"

#include <ctime>
#include <filesystem>
#include <fstream>
#include <stdexcept>
#include <string>

static bool FLAG_DOWNLOAD_ALL_PARTS = false;
static unsigned FLAG_PART_ID        = 1;
static std::string FLAG_OUTPUT_PATH;

static void perform(
    bilibiliutils::ASSConvertContextBuilder &CTXBuilder,
    bilibiliutils::Video &Video) {
  std::ofstream SummaryFile(".danmakuconv");
  std::ostream_iterator<char> SummaryFileOut(SummaryFile);
  auto Time      = std::time(nullptr);
  auto LocalTime = *std::localtime(&Time);
  SummaryFile << std::put_time(&LocalTime, "%Y-%m-%d %H:%M:%S") << std::endl;

  if (FLAG_DOWNLOAD_ALL_PARTS) {
    auto PartsCount = Video.getParts().size();
    for (std::size_t I = 0; I < PartsCount; ++I) {
      auto &&Part   = Video.getParts()[I];
      auto IndexStr = bilibiliutils::utils::formatIndex(PartsCount, I + 1);

      std::string FileName;
      if (PartsCount != 1) {
        FileName = fmt::format("{} - {}.ass", IndexStr, Part.getTitle());
      } else {
        FileName = fmt::format("{}.ass", Video.getTitle());
      }

      fmt::print("Working on {}...\n", FileName);
      CTXBuilder.setTitle(Part.getTitle());
      auto CTX = CTXBuilder.build();
      bilibiliutils::ASSConverter Converter(&CTX);
      auto Danmakus     = Part.getDanmakus();
      auto SuccessCount = Converter.push_back(Danmakus.begin(), Danmakus.end());
      std::ofstream OutputStream(FileName);
      Converter.write(OutputStream);
      fmt::print(
          "Total: {}, Skipped: {}\n",
          SuccessCount,
          Danmakus.size() - SuccessCount);
    }
  } else {
    if (!((FLAG_PART_ID >= 1) && (FLAG_PART_ID <= Video.getParts().size())))
      throw std::runtime_error("invalid part id");
    auto &&Part   = Video.getParts()[FLAG_PART_ID - 1];
    auto FileName = (FLAG_OUTPUT_PATH.empty())
                        ? fmt::format("{}.ass", Video.getTitle())
                        : FLAG_OUTPUT_PATH;
    fmt::print("Working on {}...\n", FileName);
    CTXBuilder.setTitle(Video.getTitle());
    auto CTX = CTXBuilder.build();
    bilibiliutils::ASSConverter Converter(&CTX);
    auto Danmakus     = Part.getDanmakus();
    auto SuccessCount = Converter.push_back(Danmakus.begin(), Danmakus.end());
    std::ofstream OutputStream(FileName);
    Converter.write(OutputStream);
    fmt::print(
        "Total: {}, Skipped: {}\n",
        SuccessCount,
        Danmakus.size() - SuccessCount);
  }
}

int main(int argc, char const *argv[]) {
  // clang-format off
  cxxopts::Options Options(
      "danmakuconv",
      "a simple ass subtitle downloader for bilibili.com");
  Options.add_options()
    ("h,help"  , "Show this help information")
    ("c,config", "Use config file",
      cxxopts::value<std::string>())
    ("a,all"   , "Download all parts")
    ("avid"    , "Search video with AvID (av[0-9]+)",
      cxxopts::value<std::string>())
    ("bvid"    , "Search video with BvID (BV[0-9a-zA-Z]+)",
      cxxopts::value<std::string>())
    ("p,part"  , "Select specific part (ignored when --all is used)",
      cxxopts::value<unsigned>())
    ("o"       , "Set output file path (ignored when --all is used)",
      cxxopts::value<std::string>())
  ;
  auto Result = Options.parse(argc, argv);
  // clang-format on

  if (Result.count("help") || (argc == 1)) {
    fmt::print("{}\n", Options.help());
    return EXIT_SUCCESS;
  }

  bilibiliutils::HTTPClient HTTPClient;
  bilibiliutils::ASSConvertContextBuilder CTXBuilder;

  if (Result.count("config")) {
    auto ConfigPathString = Result["config"].as<std::string>();
    std::filesystem::path ConfigPath(ConfigPathString);
    if (!is_regular_file(ConfigPath)) {
      auto Message =
          fmt::format("cannot find config file {}", ConfigPathString);
      throw std::runtime_error(Message);
    }
    HTTPClient.loadConfig(ConfigPathString);
    CTXBuilder.loadConfig(ConfigPathString);
  }

  if (Result.count("all")) FLAG_DOWNLOAD_ALL_PARTS = true;

  if (Result.count("o")) FLAG_OUTPUT_PATH = Result["o"].as<std::string>();
  if (Result.count("part")) FLAG_PART_ID = Result["part"].as<unsigned>();

  if ((!Result.count("avid") && !Result.count("bvid")) ||
      (Result.count("avid") && Result.count("bvid")) ||
      (Result.count("avid") > 1) || (Result.count("bvid") > 1)) {
    throw std::runtime_error("must provide a single AvID or BvID");
  }

  if (Result.count("avid") == 1) {
    auto AvID =
        bilibiliutils::utils::checkAvID(Result["avid"].as<std::string>());
    auto Video = bilibiliutils::Video::fromAvID(&HTTPClient, AvID);
    perform(CTXBuilder, Video);
  }

  if (Result.count("bvid") == 1) {
    auto BvID =
        bilibiliutils::utils::checkBvID(Result["bvid"].as<std::string>());
    auto Video = bilibiliutils::Video::fromBvID(&HTTPClient, BvID);
    perform(CTXBuilder, Video);
  }

  return 0;
}
