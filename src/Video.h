#ifndef BILIBILIUTILS_VIDEO_H
#define BILIBILIUTILS_VIDEO_H

#include <ctime>
#include <iterator>
#include <memory>
#include <string>
#include <string_view>

#include <fmt/format.h>
#include <nlohmann/json.hpp>
#include <range/v3/view/all.hpp>

#include "Danmaku.h"
#include "HTTPClient.h"

namespace bilibiliutils {
class Video;
class VideoPart;
class VideoStream;
class AudioStream;

class Video {
  HTTPClient *mHTTPClient;

  unsigned mAvID;
  std::string mBvID;

  std::string mTitle;
  std::string mDescription;
  std::string mCoverPicURL;

  std::time_t mPublishTime;
  std::time_t mSubmitTime;

  unsigned mOwnerMID;
  std::string mOwnerName;
  std::string mOwnerPicURL;

  std::vector<VideoPart> mParts;

  explicit Video(HTTPClient *Client, std::string_view Response);

public:
  unsigned getAvID() const { return mAvID; }
  std::string_view getBvID() const { return mBvID; }

  std::string_view getTitle() const { return mTitle; }
  std::string_view getDescription() const { return mDescription; }
  std::string_view getCoverPicURL() const { return mCoverPicURL; }

  std::time_t getPublishTime() const { return mPublishTime; }
  std::time_t getSubmitTime() const { return mSubmitTime; }

  unsigned getOwnerMID() const { return mOwnerMID; }
  std::string_view getOwnerName() const { return mOwnerName; }
  std::string_view getOwnerPicURL() const { return mOwnerPicURL; }

  auto getParts() { return ranges::views::all(mParts); }
  auto getParts() const { return ranges::views::all(mParts); }

  HTTPClient *getHTTPClient() const { return mHTTPClient; }

  static Video fromAvID(HTTPClient *Client, unsigned AvID);
  static Video fromBvID(HTTPClient *Client, std::string_view BvID);
  static Video fromURL(HTTPClient *Client, std::string_view URL);
};

class VideoPart {
  HTTPClient *mHTTPClient;
  std::string mBvID;

  unsigned mIndex;
  unsigned mCID;
  std::string mTitle;
  unsigned mDuration;

  friend class Video;
  VideoPart(
      HTTPClient *HTTPClient, std::string_view BvID,
      nlohmann::json const &PageObject);

  bool mEnableHDR         = false;
  bool mEnable4K          = true;
  bool mEnableDolbyAudio  = false;
  bool mEnableDolbyVision = false;
  bool mEnable8K          = true;
  bool mEnableAV1Codec    = true;

  std::shared_ptr<std::vector<VideoStream>> mVideoStreams;
  std::shared_ptr<std::vector<AudioStream>> mAudioStreams;
  std::shared_ptr<std::vector<Danmaku>> mDanmakus;

  unsigned getFnValFlag() const;

public:
  unsigned getIndex() const { return mIndex; }
  unsigned getCID() const { return mCID; }
  std::string_view getTitle() const { return mTitle; }

  unsigned getDuration() const { return mDuration; }
  unsigned getDanmakuSegmentCount() const;

  // clang-format off
  void setEnableHDR        (bool Enable) { mEnableHDR = Enable; }
  void setEnable4K         (bool Enable) { mEnable4K = Enable; }
  void setEnableDolbyAudio (bool Enable) { mEnableDolbyAudio = Enable; }
  void setEnableDolbyVision(bool Enable) { mEnableDolbyVision = Enable; }
  void setEnable8K         (bool Enable) { mEnable8K = Enable; }
  void setEnableAV1Codec   (bool Enable) { mEnableAV1Codec = Enable; }
  // clang-format on
  void refreshVideoAudioStreamInfo();

  auto getVideoStreams() {
    if (mVideoStreams == nullptr) refreshVideoAudioStreamInfo();
    return ranges::views::all(*mVideoStreams);
  }

  auto getAudioStreams() {
    if (mAudioStreams == nullptr) refreshVideoAudioStreamInfo();
    return ranges::views::all(*mAudioStreams);
  }

  void refreshDanmakus();

  auto getDanmakus() {
    if (mDanmakus == nullptr) refreshDanmakus();
    return ranges::views::all(*mDanmakus);
  }

  HTTPClient *getHTTPClient() const { return mHTTPClient; }
};

enum class VideoStreamQuality {
  // clang-format off
  VIDEO_240P       =   6,
  VIDEO_360P       =  16,
  VIDEO_480P       =  32,
  VIDEO_720P       =  64,
  VIDEO_720P60     =  74,
  VIDEO_1080P      =  80,
  VIDEO_1080P_PLUS = 112,
  VIDEO_1080P60    = 116,
  VIDEO_4K         = 120,
  VIDEO_HDR        = 125,
  VIDEO_DOLBY      = 126,
  VIDEO_8K         = 127,
  // clang-format on
};

enum class AudioStreamQuality {
  // clang-format off
  AUDIO_64K        = 30216,
  AUDIO_132K       = 30232,
  AUDIO_192K       = 30280,
  // clang-format on
};

VideoStreamQuality parseVideoStreamQualityCode(unsigned Code);
AudioStreamQuality parseAudioStreamQualityCode(unsigned Code);

class VideoStream {
  HTTPClient *mHTTPClient;
  VideoStreamQuality mQuality;
  std::string mURL;
  std::string mMIMEType;
  std::string mCodec;
  unsigned mWidth;
  unsigned mHeight;

  std::string mFrameRate;

  friend class VideoPart;
  VideoStream(HTTPClient *HTTPClient, nlohmann::json const &VideoObject);

public:
  VideoStreamQuality getQuality() const { return mQuality; }
  std::string_view getURL() const { return mURL; }
  std::string_view getMIMEType() const { return mMIMEType; }
  std::string_view getCodec() const { return mCodec; }
  unsigned getWidth() const { return mWidth; }
  unsigned getHeight() const { return mHeight; }
  std::string_view getFrameRate() const { return mFrameRate; }

  bool isH264() const;
  bool isH265() const;
  bool isAV1() const;

  HTTPClient *getHTTPClient() const { return mHTTPClient; }
};

class AudioStream {
  HTTPClient *mHTTPClient;
  AudioStreamQuality mQuality;
  std::string mURL;
  std::string mMIMEType;
  std::string mCodec;

  friend class VideoPart;
  AudioStream(HTTPClient *HTTPClient, nlohmann::json const &AudioObject);

public:
  AudioStreamQuality getQuality() const { return mQuality; }
  std::string_view getURL() const { return mURL; }
  std::string_view getMIMEType() const { return mMIMEType; }
  std::string_view getCodec() const { return mCodec; }

  HTTPClient *getHTTPClient() const { return mHTTPClient; }
};
} // namespace bilibiliutils
#endif
