#include <aria2/aria2.h>
#include <cxxopts.hpp>
#include <fmt/color.h>
#include <fmt/format.h>
#include <indicators/block_progress_bar.hpp>
#include <indicators/color.hpp>
#include <indicators/cursor_control.hpp>
#include <indicators/dynamic_progress.hpp>
#include <indicators/font_style.hpp>
#include <range/v3/view/filter.hpp>

#include "HTTPClient.h"
#include "Utils.h"
#include "Video.h"

#include <cstddef>
#include <cstdlib>
#include <filesystem>
#include <fstream>
#include <stdexcept>
#include <string>
#include <unordered_map>

namespace fmt {
template <> struct formatter<bilibiliutils::VideoStreamQuality> {
  using VideoStreamQuality = bilibiliutils::VideoStreamQuality;

  static constexpr std::string_view toString(VideoStreamQuality const &Value) {
    // clang-format off
    switch (Value) {
    case VideoStreamQuality::VIDEO_240P:       return "240P   ";
    case VideoStreamQuality::VIDEO_360P:       return "360P   ";
    case VideoStreamQuality::VIDEO_480P:       return "480P   ";
    case VideoStreamQuality::VIDEO_720P:       return "720P   ";
    case VideoStreamQuality::VIDEO_720P60:     return "720P60 ";
    case VideoStreamQuality::VIDEO_1080P:      return "1080P  ";
    case VideoStreamQuality::VIDEO_1080P_PLUS: return "1080P+ ";
    case VideoStreamQuality::VIDEO_1080P60:    return "1080P60";
    case VideoStreamQuality::VIDEO_4K:         return "4K     ";
    case VideoStreamQuality::VIDEO_HDR:        return "HDR    ";
    case VideoStreamQuality::VIDEO_DOLBY:      return "Dolby  ";
    case VideoStreamQuality::VIDEO_8K:         return "8K     ";
    default: throw std::logic_error("unknown VideoStreamQuality enum");
    }
    // clang-format on
  }

  template <typename Context> constexpr auto parse(Context &&C) {
    return C.begin();
  }

  template <typename Context>
  constexpr auto format(VideoStreamQuality const &Value, Context &&C) {
    return fmt::format_to(C.out(), "{}", toString(Value));
  }
};

template <> struct formatter<bilibiliutils::AudioStreamQuality> {
  using AudioStreamQuality = bilibiliutils::AudioStreamQuality;

  static constexpr std::string_view toString(AudioStreamQuality const &Value) {
    // clang-format off
    switch (Value) {
    case AudioStreamQuality::AUDIO_64K:  return "64K    ";
    case AudioStreamQuality::AUDIO_132K: return "132K   ";
    case AudioStreamQuality::AUDIO_192K: return "192K   ";
    default: throw std::logic_error("unknown AudioStreamQuality enum");
    }
    // clang-format on
  }

  template <typename Context> constexpr auto parse(Context &&C) {
    return C.begin();
  }

  template <typename Context>
  constexpr auto format(AudioStreamQuality const &Value, Context &&C) {
    return fmt::format_to(C.out(), "{}", toString(Value));
  }
};
} // namespace fmt

static bool FLAG_DOWNLOAD_ALL_PARTS = false;
static bool FLAG_ONLY_USE_H264      = false;
static bool FLAG_ONLY_USE_H265      = false;
static bool FLAG_ONLY_USE_AV1       = false;
static bool FLAG_FALLBACK           = false;
static bool FLAG_COVER_PIC_ONLY     = false;
static bool FLAG_DESCRIPTION_ONLY   = false;
static unsigned FLAG_PART_ID        = 1;
static std::string FLAG_OUTPUT_PATH_VIDEO;
static std::string FLAG_OUTPUT_PATH_AUDIO;

static void dumpVideoInfos(
    bilibiliutils::VideoPart &VideoPart,
    bilibiliutils::VideoStream const &SelectedVideoStream,
    bilibiliutils::AudioStream const &SelectedAudioStream,
    std::string_view VideoStreamFileName,
    std::string_view AudioStreamFileName) {
  fmt::print(fmt::emphasis::underline, "Video {}:\n", VideoStreamFileName);
  for (auto &&Stream : VideoPart.getVideoStreams()) {
    if (&Stream == &SelectedVideoStream) {
      fmt::print(
          fmt::emphasis::bold | fg(fmt::color::green),
          "(*){} {} {}\n",
          Stream.getQuality(),
          Stream.getMIMEType(),
          Stream.getCodec());
    } else {
      fmt::print(
          "   {} {} {}\n",
          Stream.getQuality(),
          Stream.getMIMEType(),
          Stream.getCodec());
    }
  }
  fmt::print(fmt::emphasis::underline, "Audio {}:\n", AudioStreamFileName);
  for (auto &&Stream : VideoPart.getAudioStreams()) {
    if (&Stream == &SelectedAudioStream) {
      fmt::print(
          fmt::emphasis::bold | fg(fmt::color::green),
          "(*){} {} {}\n",
          Stream.getQuality(),
          Stream.getMIMEType(),
          Stream.getCodec());
    } else {
      fmt::print(
          "   {} {} {}\n",
          Stream.getQuality(),
          Stream.getMIMEType(),
          Stream.getCodec());
    }
  }
  fmt::print("\n");
}

static bilibiliutils::VideoStream const &
selectVideoStream(bilibiliutils::VideoPart &VideoPart) {
  // clang-format off
  auto Streams = VideoPart.getVideoStreams()
    | ranges::views::filter(
      [&](auto &&Stream) {
        if (Stream.isH264() && FLAG_ONLY_USE_H264) return true;
        if (Stream.isH265() && FLAG_ONLY_USE_H265) return true;
        if (Stream.isAV1()  && FLAG_ONLY_USE_AV1 ) return true;
        return !(FLAG_ONLY_USE_AV1 || FLAG_ONLY_USE_H264 || FLAG_ONLY_USE_H265);
      });
  // clang-format on
  if (Streams.empty()) {
    if (FLAG_FALLBACK) {
      return VideoPart.getVideoStreams().front();
    } else {
      throw std::runtime_error("cannot find any video stream");
    }
  }
  return Streams.front();
}

bool isFallbackFormat(bilibiliutils::VideoStream const &Stream) {
  if (FLAG_ONLY_USE_AV1 && !Stream.isAV1()) return true;
  if (FLAG_ONLY_USE_H264 && !Stream.isH264()) return true;
  if (FLAG_ONLY_USE_H265 && !Stream.isH265()) return true;
  return false;
}

static bilibiliutils::AudioStream const &
selectAudioStream(bilibiliutils::VideoPart &VideoPart) {
  auto Streams = VideoPart.getAudioStreams();
  if (Streams.empty()) {
    throw std::runtime_error("cannot find any audio stream");
  }
  return Streams.front();
}

static std::unique_ptr<indicators::BlockProgressBar>
createProgressBar(std::string_view Title) {
  using namespace indicators;
  auto ProgressBar = std::make_unique<indicators::BlockProgressBar>();
  ProgressBar->set_option(
      option::FontStyles{std::vector<FontStyle>{FontStyle::bold}});
  ProgressBar->set_option(option::ForegroundColor(Color::yellow));
  ProgressBar->set_option(option::PrefixText(Title));
  ProgressBar->set_option(option::ShowPercentage(true));
  ProgressBar->set_option(option::ShowElapsedTime(true));
  ProgressBar->set_option(option::ShowRemainingTime(true));
  ProgressBar->set_option(option::BarWidth(30));
  ProgressBar->set_option(option::MaxPostfixTextLen(20));
  return ProgressBar;
}

static void perform(bilibiliutils::Video &Video) {
  using namespace indicators;
  using Aria2DownloadTask = bilibiliutils::HTTPClient::Aria2DownloadTask;
  auto *ClientPtr         = Video.getHTTPClient();
  DynamicProgress<BlockProgressBar> ProgressBarGroup;
  std::vector<std::unique_ptr<BlockProgressBar>> ProgressBars;
  std::unordered_map<Aria2DownloadTask *, std::size_t> ProgressBarMap;

  std::vector<std::string> FallbackVideoStreamNames;

  std::ofstream DescriptionFile(".description");
  std::ostream_iterator<char> DescriptionFileOut(DescriptionFile);
  fmt::format_to(DescriptionFileOut, "{}\n", Video.getDescription());
  fmt::print("{}\n\n", Video.getDescription());
  if (FLAG_DESCRIPTION_ONLY) return;

  auto *CoverPicTask =
      ClientPtr->addAria2Task(Video.getCoverPicURL(), "cover.jpg");
  auto CoverPicBar   = createProgressBar("Cover ");
  auto CoverPicIndex = ProgressBarGroup.push_back(*CoverPicBar);
  ProgressBars.push_back(std::move(CoverPicBar));
  ProgressBarMap.insert({CoverPicTask, CoverPicIndex});

  if (!FLAG_COVER_PIC_ONLY) {
    std::ofstream SummaryFile(".bilibili-dl");
    std::ostream_iterator<char> SummaryFileOut(SummaryFile);

    if (FLAG_DOWNLOAD_ALL_PARTS) {
      auto PartsCount = Video.getParts().size();
      for (std::size_t I = 0; I < PartsCount; ++I) {
        auto &&Part        = Video.getParts()[I];
        auto &&VideoStream = selectVideoStream(Part);
        auto &&AudioStream = selectAudioStream(Part);
        auto IndexStr = bilibiliutils::utils::formatIndex(PartsCount, I + 1);

        std::string VFileName, AFileName;
        if (PartsCount != 1) {
          VFileName = fmt::format("{} - {}.m4v", IndexStr, Part.getTitle());
          AFileName = fmt::format("{} - {}.m4a", IndexStr, Part.getTitle());
        } else {
          VFileName = fmt::format("{}.m4v", Video.getTitle());
          AFileName = fmt::format("{}.m4a", Video.getTitle());
        }
        if (isFallbackFormat(VideoStream))
          FallbackVideoStreamNames.push_back(VFileName);
        fmt::format_to(
            SummaryFileOut,
            "{}\n{} {} {}\n",
            VFileName,
            VideoStream.getMIMEType(),
            VideoStream.getQuality(),
            VideoStream.getCodec());
        fmt::format_to(
            SummaryFileOut,
            "{}\n{} {} {}\n",
            AFileName,
            AudioStream.getMIMEType(),
            AudioStream.getQuality(),
            AudioStream.getCodec());

        dumpVideoInfos(Part, VideoStream, AudioStream, VFileName, AFileName);
        auto *VTask = ClientPtr->addAria2Task(VideoStream.getURL(), VFileName);
        auto *ATask = ClientPtr->addAria2Task(AudioStream.getURL(), AFileName);
        auto VBar   = createProgressBar(fmt::format("Video {} ", IndexStr));
        auto VBarIndex = ProgressBarGroup.push_back(*VBar);
        ProgressBars.push_back(std::move(VBar));
        ProgressBarMap.insert({VTask, VBarIndex});
        auto ABar      = createProgressBar(fmt::format("Audio {} ", IndexStr));
        auto ABarIndex = ProgressBarGroup.push_back(*ABar);
        ProgressBars.push_back(std::move(ABar));
        ProgressBarMap.insert({ATask, ABarIndex});
      }
    } else {
      if (!((FLAG_PART_ID >= 1) && (FLAG_PART_ID <= Video.getParts().size())))
        throw std::runtime_error("invalid part id");
      auto &&Part        = Video.getParts()[FLAG_PART_ID - 1];
      auto &&VideoStream = selectVideoStream(Part);
      auto &&AudioStream = selectAudioStream(Part);

      auto VFileName = FLAG_OUTPUT_PATH_VIDEO.empty()
                           ? fmt::format("{}.m4v", Video.getTitle())
                           : FLAG_OUTPUT_PATH_VIDEO;
      auto AFileName = FLAG_OUTPUT_PATH_AUDIO.empty()
                           ? fmt::format("{}.m4a", Video.getTitle())
                           : FLAG_OUTPUT_PATH_AUDIO;
      if (isFallbackFormat(VideoStream))
        FallbackVideoStreamNames.push_back(VFileName);
      fmt::format_to(
          SummaryFileOut,
          "{}\n  {} {} {}\n",
          VFileName,
          VideoStream.getQuality(),
          VideoStream.getMIMEType(),
          VideoStream.getCodec());
      fmt::format_to(
          SummaryFileOut,
          "{}\n {} {} {}\n",
          AFileName,
          AudioStream.getQuality(),
          AudioStream.getMIMEType(),
          AudioStream.getCodec());

      dumpVideoInfos(Part, VideoStream, AudioStream, VFileName, AFileName);
      auto *VTask    = ClientPtr->addAria2Task(VideoStream.getURL(), VFileName);
      auto *ATask    = ClientPtr->addAria2Task(AudioStream.getURL(), AFileName);
      auto VBar      = createProgressBar("Video ");
      auto VBarIndex = ProgressBarGroup.push_back(*VBar);
      ProgressBars.push_back(std::move(VBar));
      ProgressBarMap.insert({VTask, VBarIndex});
      auto ABar      = createProgressBar("Audio ");
      auto ABarIndex = ProgressBarGroup.push_back(*ABar);
      ProgressBars.push_back(std::move(ABar));
      ProgressBarMap.insert({ATask, ABarIndex});
    }
  }

  show_console_cursor(false);
  ClientPtr->runAria2Tasks(
      [&ProgressBarGroup, &ProgressBarMap](
          Aria2DownloadTask *TaskPtr, aria2::DownloadHandle *Aria2Handle) {
        auto Status        = Aria2Handle->getStatus();
        auto ContentLength = Aria2Handle->getTotalLength();
        auto &ProgressBar  = ProgressBarGroup[ProgressBarMap[TaskPtr]];
        ProgressBar.set_option(option::MaxProgress(ContentLength));
        if (Status == aria2::DownloadStatus::DOWNLOAD_COMPLETE) {
          ProgressBar.set_option(option::ForegroundColor(Color::green));
          ProgressBar.set_option(option::PostfixText("COMPLETE"));
          ProgressBar.set_progress(static_cast<float>(ContentLength));
          ProgressBar.mark_as_completed();
        } else if (Status == aria2::DownloadStatus::DOWNLOAD_ACTIVE) {
          auto SpeedStr = bilibiliutils::utils::formatHumanReadableSpeed(
              Aria2Handle->getDownloadSpeed());
          auto ConnectionCount = Aria2Handle->getConnections();
          auto PostStr = fmt::format("CN: {} {}", ConnectionCount, SpeedStr);
          ProgressBar.set_option(option::PostfixText(PostStr));
          ProgressBar.set_progress(
              static_cast<float>(Aria2Handle->getCompletedLength()));
        } else if (Status == aria2::DOWNLOAD_ERROR) {
          ProgressBar.set_option(option::ForegroundColor(Color::red));
          ProgressBar.set_option(option::PostfixText("ERROR"));
        }
        ProgressBarGroup.print_progress();
      });
  show_console_cursor(true);

  if (!FallbackVideoStreamNames.empty()) {
    fmt::print("\n");
    fmt::print(
        fmt::emphasis::bold | fg(fmt::color::red), "Using fallback streams:\n");
    for (auto &&StreamFName : FallbackVideoStreamNames) {
      fmt::print("  {}\n", StreamFName);
    }
  }
}

int main(int argc, char const *argv[]) {
  // clang-format off
  cxxopts::Options Options(
      "bilibili-dl",
      "a simple video downloader for bilibili.com");
  Options.add_options()
    ("h,help"  , "Show this help information")
    ("c,config", "Use config file",
      cxxopts::value<std::string>())
    ("a,all"   , "Download all parts")
    ("avid"    , "Search video with AvID (av[0-9]+)",
      cxxopts::value<std::string>())
    ("bvid"    , "Search video with BvID (BV[0-9a-zA-Z]+)",
      cxxopts::value<std::string>())
    ("p,part"  , "Select specific part (ignored when --all is used)",
      cxxopts::value<unsigned>())
    ("cover-pic-only", "Only download the cover picture for the video")
    ("description-only", "Only fetch the description for the video")
    ("h264"    , "Use H264 video stream only")
    ("h265"    , "Use H265 video stream only")
    ("av1"     , "Use AV1 video stream only" )
    ("fallback", "Fallback to best video stream if matched stream found")
    ("vo"       , "Set output path for video (ignored when --all is used)",
     cxxopts::value<std::string>())
    ("ao"       , "Set output path for audio (ignored when --all is used)",
     cxxopts::value<std::string>())
  ;
  auto Result = Options.parse(argc, argv);
  // clang-format on

  if (Result.count("help") || (argc == 1)) {
    fmt::print("{}\n", Options.help());
    return EXIT_SUCCESS;
  }

  bilibiliutils::HTTPClient HTTPClient;

  if (Result.count("config")) {
    auto ConfigPathString = Result["config"].as<std::string>();
    std::filesystem::path ConfigPath(ConfigPathString);
    if (!is_regular_file(ConfigPath)) {
      auto Message =
          fmt::format("cannot find config file {}", ConfigPathString);
      throw std::runtime_error(Message);
    }
    HTTPClient.loadConfig(ConfigPathString);
  }

  if (Result.count("all")) FLAG_DOWNLOAD_ALL_PARTS = true;
  if (Result.count("cover-pic-only")) FLAG_COVER_PIC_ONLY = true;
  if (Result.count("description-only")) FLAG_DESCRIPTION_ONLY = true;
  if (Result.count("h264")) FLAG_ONLY_USE_H264 = true;
  if (Result.count("h265")) FLAG_ONLY_USE_H265 = true;
  if (Result.count("av1")) FLAG_ONLY_USE_AV1 = true;
  if (Result.count("fallback")) FLAG_FALLBACK = true;

  if (Result.count("vo"))
    FLAG_OUTPUT_PATH_VIDEO = Result["vo"].as<std::string>();
  if (Result.count("ao"))
    FLAG_OUTPUT_PATH_AUDIO = Result["ao"].as<std::string>();
  if (Result.count("part")) FLAG_PART_ID = Result["part"].as<unsigned>();

  if ((!Result.count("avid") && !Result.count("bvid")) ||
      (Result.count("avid") && Result.count("bvid")) ||
      (Result.count("avid") > 1) || (Result.count("bvid") > 1)) {
    throw std::runtime_error("must provide a single AvID or BvID");
  }

  if (Result.count("avid") == 1) {
    auto AvID =
        bilibiliutils::utils::checkAvID(Result["avid"].as<std::string>());
    auto Video = bilibiliutils::Video::fromAvID(&HTTPClient, AvID);
    perform(Video);
  }

  if (Result.count("bvid") == 1) {
    auto BvID =
        bilibiliutils::utils::checkBvID(Result["bvid"].as<std::string>());
    auto Video = bilibiliutils::Video::fromBvID(&HTTPClient, BvID);
    perform(Video);
  }

  return EXIT_SUCCESS;
}
