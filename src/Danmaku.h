#ifndef BILIBILIUTILS_DANMAKU_H
#define BILIBILIUTILS_DANMAKU_H

#include "DanmakuAPI.pb.h"

#include <cstdint>
#include <ctime>
#include <string>
#include <string_view>

namespace bilibiliutils {

class Danmaku {
public:
  enum class Modes { Normal, Bottom, Top, Reverse, FX, Code, BAS };
  enum class Pools { Normal, Subtitle, Special };

  class Color {
    std::uint8_t mR, mG, mB;

  public:
    Color() : mR(0), mG(0), mB(0) {}
    Color(std::uint8_t R, std::uint8_t G, std::uint8_t B) noexcept
        : mR(R), mG(G), mB(B) {}
    static Color fromRGB888(std::uint32_t Code);
    std::uint8_t getRed() const { return mR; }
    std::uint8_t getGreen() const { return mG; }
    std::uint8_t getBlue() const { return mB; }

    bool operator==(Color const &other) const noexcept {
      return (mR == other.mR) && (mG == other.mG) && (mB == other.mB);
    }
    bool operator!=(Color const &o) const noexcept { return !(*this == o); }

    static Color WHITE;
    static Color BLACK;
  };

private:
  std::uint64_t mID;
  std::uint32_t mProgress;
  Modes mMode;
  std::uint32_t mFontSize;
  Color mColor;
  std::string mMidHash;
  std::string mContent;
  std::time_t mSubmitTime;
  std::uint32_t mWeight;
  Pools mPool;

public:
  using ProtoBufObject = bilibili::community::service::dm::v1::DanmakuElem;
  explicit Danmaku(ProtoBufObject const &ProtoBufElem);

  std::uint64_t getID() const { return mID; }
  std::uint32_t getProgress() const { return mProgress; }
  Modes getMode() const { return mMode; }
  std::uint32_t getFontSize() const { return mFontSize; }
  Color getColor() const { return mColor; }
  std::string_view getMidHash() const { return mMidHash; }
  std::string_view getContent() const { return mContent; }
  std::time_t getSubmitTime() const { return mSubmitTime; }
  std::uint32_t getWeight() const { return mWeight; }
  Pools getPool() const { return mPool; }

  unsigned getWidth() const;
  unsigned getHeight() const;
};
} // namespace bilibiliutils

#endif